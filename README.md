# La Buena Pinta

Binevenid@ al repositorio de La Buena Pinta, programa destinado a la gestión de tiendas de cervezas.  
El programa está desarrollado con Java utilizando Swing para la interfaz gráfica, MariaDB para la base de datos y JDBC como interfaz de comunicación.  
Este programa forma parte del proyecto de la asignatura Ingeniería de Software de la Universidad Complutense de Madrid(UCM).  
Está diseñado utilizando la Arquitectura Multicapa y un repertorio de patrones OO.

He aquí el Diagrama ER:

![Diagrama ER](https://gitlab.com/LaBuenaPinta/LaBuenaPinta/raw/master/BBDD/ER.png)

